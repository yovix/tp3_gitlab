#include "main.c"
#include <assert.h>

int test_integration() {
    // Tester l'intégration des fonctions hasard et comparaison avec le programme principal
    char choix_joueur = 'R';
    char choix_ordi = hasard();
    int resultat = comparaison(choix_joueur, choix_ordi);
    assert(resultat == -1 || resultat == 0 || resultat == 1);
    return 0;
}
