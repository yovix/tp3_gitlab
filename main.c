#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

// Fonction hasard pour simuler le choix de l'ordinateur
char hasard() {
    char choix_ordi;
    int random_num = rand() % 3;
    if (random_num == 0) {
        choix_ordi = 'R';
    } else if (random_num == 1) {
        choix_ordi = 'P';
    } else {
        choix_ordi = 'C';
    }
    return choix_ordi;
}

// Fonction comparaison pour déterminer le résultat du jeu
int comparaison(char choix_joueur, char choix_ordi) {
    if (choix_joueur == choix_ordi) {
        return 0; // Égalité
    } else if ((choix_joueur == 'R' && choix_ordi == 'C') || 
               (choix_joueur == 'P' && choix_ordi == 'R') || 
               (choix_joueur == 'C' && choix_ordi == 'P')) {
        return 1; // Le joueur gagne
    } else {
        return -1; // L'ordinateur gagne
    }
}

int main(int argc,char *argv[]) {
//int main() {
    srand(time(NULL)); // Initialisation du générateur de nombres aléatoires
    //char choix_joueur, choix_ordi;
    char choix_ordi;
    int resultat;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s [R|P|C]\n", argv[0]);
        return 1;
    }
    char choix_joueur = argv[1][0];


    // Saisie du choix du joueur
    printf("Choisissez Roche (R), Papier (P), ou Ciseaux (C): ");
    scanf(" %c", &choix_joueur);

    // Valider le choix du joueur
    assert(choix_joueur == 'R' || choix_joueur == 'P' || choix_joueur == 'C');

    // Obtenir le choix de l'ordinateur
    choix_ordi = hasard();

    // Affichage des choix
    printf("Joueur choisit: %c\n", choix_joueur);
    printf("Ordinateur choisit: %c\n", choix_ordi);

    // Comparaison des choix
    resultat = comparaison(choix_joueur, choix_ordi);

    // Affichage du résultat
    if (resultat == 0) {
        printf("Égalité !\n");
    } else if (resultat == 1) {
        printf("Le joueur gagne !\n");
    } else {
        printf("L'ordinateur gagne !\n");
    }

    return 0;
}
