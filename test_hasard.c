#include <assert.h>
#include "main.c"

int test_hasard() {
    char choix_ordi = hasard();
    assert(choix_ordi == 'R' || choix_ordi == 'P' || choix_ordi == 'C');
    return 0;
}
