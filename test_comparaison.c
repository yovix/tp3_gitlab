#include "main.c"
#include <assert.h>

int test_comparaison() {
    assert(comparaison('R', 'C') == 1); // Le joueur gagne
    assert(comparaison('P', 'R') == 1); // Le joueur gagne
    assert(comparaison('C', 'P') == 1); // Le joueur gagne
    assert(comparaison('R', 'R') == 0); // Égalité
    assert(comparaison('P', 'C') == -1); // L'ordinateur gagne
    assert(comparaison('C', 'R') == -1); // L'ordinateur gagne
    return 0;
}
